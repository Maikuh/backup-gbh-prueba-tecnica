import {
    Entity,
    PrimaryGeneratedColumn,
    BaseEntity,
    Column,
    CreateDateColumn,
    ManyToOne,
} from "typeorm";
import { Book } from "./Book";

@Entity()
export class BookPage extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false,
    })
    number: number;

    @Column({
        nullable: false,
    })
    contents: string;

    @Column({
        enum: ["text", "html"],
    })
    format: string;

    @ManyToOne((type) => Book, (book) => book.pages)
    book: Book;

    @CreateDateColumn()
    createdAt: Date;
}
