import {
    Entity,
    PrimaryGeneratedColumn,
    BaseEntity,
    Column,
    CreateDateColumn,
    OneToMany
} from "typeorm";
import { Book } from "./Book";

@Entity()
export class Author extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false
    })
    name: string;

    @OneToMany(type => Book, book => book.author)
    books: Book[]

    @CreateDateColumn()
    createdAt: Date;
}
