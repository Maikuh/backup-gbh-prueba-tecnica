import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, CreateDateColumn, ManyToMany, JoinTable, OneToMany, ManyToOne } from "typeorm";
import { Genre } from "./Genre";
import { BookPage } from "./BookPage";
import { Author } from "./Author";

@Entity()
export class Book extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false
    })
    title: string;

    @Column({
        nullable: false
    })
    synopsis: string;

    @Column({
        unique: true,
        nullable: false
    })
    ISBN: string;

    @ManyToMany(type => Genre)
    @JoinTable()
    genres: Genre[]

    @OneToMany(type => BookPage, bookPage => bookPage.book)
    pages: BookPage[]

    @ManyToOne(type => Author, author => author.books)
    author: Author;

    @Column({
        type: "date",
        nullable: false,
    })
    publishDate: Date;

    @CreateDateColumn()
    createdAt: Date;
}