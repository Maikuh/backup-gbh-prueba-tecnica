import http from "http";
import url from "url";
import Router from "router";
import morgan from "morgan";
import finalhandler from "finalhandler";
import booksRoute from "./booksRoute";

const router = new Router();
const logger = morgan("dev");

// Set HTTP Server and Router package
const server = http.createServer((req: any, res) => {
    // Parse query strings
    const query = url.parse(req.url, true).query;
    req.query = query;

    // Setup Morgan's logger
    logger(req, res, (err) => {
        if (err) finalhandler(req, res)(err);
    });

    // Setup the Router
    router(req, res, finalhandler(req, res));
});

// Setup Routes
booksRoute(router);

export default server;
