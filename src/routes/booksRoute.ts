import booksService from "../services/booksService";

export default function (router) {
    router.get("/api/books", (req, res) => {
        const { page, take } = req.query;
        booksService.allBooks(res, Number(page), Number(take));
    });

    router.get("/api/books/:id", (req, res) => {
        const { page, format } = req.query;
        const { id } = req.params;

        booksService.singleBook(res, Number(id), Number(page), format);
    });
}
