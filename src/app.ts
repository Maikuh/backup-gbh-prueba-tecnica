import "reflect-metadata";
import database from "./database";
import "dotenv/config";
import server from "./routes";

const HOST = process.env.HOST;
const PORT: any = process.env.PORT || 3000;

(async function () {
    await database();

    server.listen(PORT, HOST, () => {
        console.log(`Server running at http://${HOST}:${PORT}`);
    });
})();
