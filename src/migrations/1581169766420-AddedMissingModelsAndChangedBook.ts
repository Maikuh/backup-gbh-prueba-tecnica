import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedMissingModelsAndChangedBook1581169766420 implements MigrationInterface {
    name = 'AddedMissingModelsAndChangedBook1581169766420'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "genre" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_0285d4f1655d080cfcf7d1ab141" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "book_page" ("id" SERIAL NOT NULL, "number" integer NOT NULL, "contents" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "bookId" integer, CONSTRAINT "PK_ea6cf4a1b8968c3481f29729b93" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "author" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_5a0e79799d372fe56f2f3fa6871" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "book_genres_genre" ("bookId" integer NOT NULL, "genreId" integer NOT NULL, CONSTRAINT "PK_75a197f32ed39286c5c39198ece" PRIMARY KEY ("bookId", "genreId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_31d658e0af554165f4598158c5" ON "book_genres_genre" ("bookId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_83bd32782d44d9db3d68c3f58c" ON "book_genres_genre" ("genreId") `, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "releaseDate"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "synopsis" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "ISBN" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD CONSTRAINT "UQ_7459018069b9c93b1d66ec013a4" UNIQUE ("ISBN")`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "publishDate" date NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "authorId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "book_page" ADD CONSTRAINT "FK_7b5fd7cb7b4c9db05ceb761d8a2" FOREIGN KEY ("bookId") REFERENCES "book"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD CONSTRAINT "FK_66a4f0f47943a0d99c16ecf90b2" FOREIGN KEY ("authorId") REFERENCES "author"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "book_genres_genre" ADD CONSTRAINT "FK_31d658e0af554165f4598158c55" FOREIGN KEY ("bookId") REFERENCES "book"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "book_genres_genre" ADD CONSTRAINT "FK_83bd32782d44d9db3d68c3f58c1" FOREIGN KEY ("genreId") REFERENCES "genre"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "book_genres_genre" DROP CONSTRAINT "FK_83bd32782d44d9db3d68c3f58c1"`, undefined);
        await queryRunner.query(`ALTER TABLE "book_genres_genre" DROP CONSTRAINT "FK_31d658e0af554165f4598158c55"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP CONSTRAINT "FK_66a4f0f47943a0d99c16ecf90b2"`, undefined);
        await queryRunner.query(`ALTER TABLE "book_page" DROP CONSTRAINT "FK_7b5fd7cb7b4c9db05ceb761d8a2"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "authorId"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "publishDate"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP CONSTRAINT "UQ_7459018069b9c93b1d66ec013a4"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "ISBN"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" DROP COLUMN "synopsis"`, undefined);
        await queryRunner.query(`ALTER TABLE "book" ADD "releaseDate" date NOT NULL`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_83bd32782d44d9db3d68c3f58c"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_31d658e0af554165f4598158c5"`, undefined);
        await queryRunner.query(`DROP TABLE "book_genres_genre"`, undefined);
        await queryRunner.query(`DROP TABLE "author"`, undefined);
        await queryRunner.query(`DROP TABLE "book_page"`, undefined);
        await queryRunner.query(`DROP TABLE "genre"`, undefined);
    }

}
