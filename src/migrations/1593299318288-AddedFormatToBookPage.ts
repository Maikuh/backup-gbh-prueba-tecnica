import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedFormatToBookPage1593299318288 implements MigrationInterface {
    name = 'AddedFormatToBookPage1593299318288'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "book_page" ADD "format" character varying NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "book_page" DROP COLUMN "format"`, undefined);
    }

}
