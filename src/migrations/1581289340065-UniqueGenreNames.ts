import {MigrationInterface, QueryRunner} from "typeorm";

export class UniqueGenreNames1581289340065 implements MigrationInterface {
    name = 'UniqueGenreNames1581289340065'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "genre" ADD CONSTRAINT "UQ_dd8cd9e50dd049656e4be1f7e8c" UNIQUE ("name")`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "genre" DROP CONSTRAINT "UQ_dd8cd9e50dd049656e4be1f7e8c"`, undefined);
    }

}
