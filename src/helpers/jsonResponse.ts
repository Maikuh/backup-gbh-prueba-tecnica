export default function (res, status, body) {
    res.statusCode = status || 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(body, null, 2));
}
