export default function() {
    let result = "";
    const characters = "0123456789";

    for (var i = 0; i < 13; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * characters.length)
        );
    }
    
    return result;
}
