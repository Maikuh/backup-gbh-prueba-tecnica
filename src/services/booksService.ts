import jsonResponse from "../helpers/jsonResponse";
import { Book } from "../models/Book";
import { ServerResponse } from "http";

export default {
    async allBooks(res: ServerResponse, page: number, take: number) {
        page = page || 1;
        take = take || 10;

        const result = await Book.findAndCount({
            relations: ["author", "genres"],
            skip: (page - 1) * take,
            take,
        });

        const body = {
            books: result[0],
            totalCount: result[1],
            page,
        };

        jsonResponse(res, 200, body);
    },
    async singleBook(
        res: ServerResponse,
        id: number,
        pageNumber: number,
        format = "text"
    ) {
        let result: any;

        if (!["html", "text"].includes(format)) {
            result = {
                message:
                    "That format doesn't exist. Please use 'text' or 'html' instead.",
            };
            return jsonResponse(res, 400, result);
        }

        result = await Book.findOne(id, {
            relations: ["author", "genres"],
            join: { alias: "book", leftJoinAndSelect: { pages: "book.pages" } },
            where: (qb) => {
                qb.where("pages.format = :format", {
                    format,
                });
            },
        });

        if (!result) {
            result = { message: "Book not found" };
            return jsonResponse(res, 404, result);
        }

        if (pageNumber > 0) {
            result.pages = result.pages.filter(
                (bookPage) => bookPage.number === pageNumber
            );
        }

        jsonResponse(res, 200, result);
    },
};
