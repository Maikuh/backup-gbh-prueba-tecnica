import { createConnection } from "typeorm";

export default async function () {
    try {
        await createConnection();
        console.log("Connected to the database");
    } catch (error) {
        console.error(error);
    }
}
