import { Author } from "../models/Author";
import faker from "faker";

export default async function() {
    let authors = [];

    for (let i = 0; i < 5; i++) {
        // Generate a fake author
        const author = Author.create({
            name: faker.name.firstName() + " " + faker.name.lastName()
        });

        authors.push(author);
    }

    authors = await Author.save(authors);

    console.log("Seeded authors...");

    return authors;
}
