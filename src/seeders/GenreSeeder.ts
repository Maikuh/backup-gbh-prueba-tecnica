import { Genre } from "../models/Genre";

export default async function() {
    let genres: any = [
        {
            name: "Action"
        },
        {
            name: "Adventure"
        },
        {
            name: "Romance"
        },
        {
            name: "Sci-fi"
        },
        {
            name: "Drama"
        }
    ];

    try {
        genres = await Genre.save(genres);
    } catch (error) {
        console.error(error)
    }

    console.log("Seeded genres...")
    return genres 
}
