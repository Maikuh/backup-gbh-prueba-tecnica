import { Book } from "../models/Book";
import randomISBN from "../helpers/randomISBN";
import AuthorSeeder from "./AuthorSeeder";
import GenreSeeder from "./GenreSeeder";
import striptags from "striptags";
import faker from "faker";
import { BookPage } from "../models/BookPage";
import database from "../database";
import fs from "fs-extra";

(async function () {
    // Connect to the database
    await database();

    // Seed dependant relations first
    const seededAuthors = await AuthorSeeder();
    const seededGenres = await GenreSeeder();
    const books = [];
    const blogpostArr: [] = await fs.readJSON(
        "src/seeders/random-blogpost-in-html.json"
    );

    for (let i = 0; i < 10; i++) {
        let bookPages: BookPage[] = [];

        // Generate book's pages first
        for (let i = 0; i < blogpostArr.length; i++) {
            const bookPageHtml = BookPage.create({
                contents: blogpostArr[i],
                number: i + 1,
                format: "html",
            });

            const bookPageText = BookPage.create({
                contents: striptags(blogpostArr[i], [], "\n"),
                number: i + 1,
                format: "text",
            });

            bookPages.push(bookPageHtml, bookPageText);
        }

        bookPages = await BookPage.save(bookPages);

        // Generate book
        const book = Book.create({
            ISBN: randomISBN(),
            author: seededAuthors[Math.floor(Math.random() * 5)],
            genres: [seededGenres[Math.floor(Math.random() * 5)]],
            publishDate: faker.date.recent(),
            synopsis: faker.lorem.paragraph(),
            pages: bookPages,
            title: faker.lorem.sentence(6),
        });

        books.push(book);
    }

    await Book.save(books);

    console.log("Seeded books...");
    console.log("Done!");
})();
