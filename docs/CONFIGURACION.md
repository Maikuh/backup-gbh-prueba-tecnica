# Configuracion y Ejecucion

## Configuracion
1. Renombrar el archivo __.env.example__ a **.env**. Configurar las variables de acuerdo a su gusto. Por defecto, el puerto es 3000.
2. Crear una base de datos con un nombre cualquiera (_gbh-biblioteca_ es el nombre por defecto utilizado en el proyecto)
3. Renombrar el archivo __ormconfig.example.json__ a **ormconfig.json**. Editar las variables de acuerdo a su entorno. Para mas informacion, visitar la [documentacion de TypeORM sobre las opciones de coneccion](https://typeorm.io/#/connection-options).
    * Por lo general, solo debera editar las variables _type_, _host_, _username_, _password_ y _database_. Notese que la base de datos utilizada en mi entorno fue PostgreSQL y el proyecto ya esta configurado para utilizar esta base de datos. En caso de querer utilizar alguna otra, instale el driver correspondiente segun el punto 4 de la seccion __Installation__ de la [documentacion de TypeORM](https://typeorm.io/#/).
    * En la variable _database_ poner el nombre de la base de datos creada en el punto 2
4. Ejecutar ```npm install``` en el directorio del proyecto para instalar los paquetes
5. Ejecutar ```npm run typeorm:migrate``` para migrar la base de datos y crear todas las tablas
6. Ejecutar ```npm run seed``` para agregar datos de prueba en la base de datos

## Ejecucion
### Metodo 1: Sin compilar a JS
1. Ejecutar ```npm run start:dev``` o ```npm run dev```
2. Ir a ```http://localhost:3000/api/books``` u otro puerto en caso de haberlo modificado
3. ... Profit?

### Metodo 2: Compilar a JS
1. Modificar el archivo **ormconfig.json**. Editar las variables _entities_ y _migrations_ para que apunten hacia ```build/models/**/*.js``` y ```build/migrations/**/*.js``` respectivamente
2. Ejecutar ```npm run tsc```
3. Ejecutar ```npm start```
4. Ir a ```http://localhost:3000/api/books``` u otro puerto en caso de haberlo modificado
5. ... Profit?