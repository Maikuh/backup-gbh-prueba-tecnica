# API

## GET: /api/books
Retorna una lista de los libros en la base de datos. Contiene funcionalidad de paginacion.

### Query Parameters
* **page:** especifica la pagina/cursor para hacer uso de la paginacion
* **take:** especifica cuantos items seran retornados por el API

Ejemplo:
```http://localhost:3000/api/books?page=1&take=10```

## GET: /api/books/:id
Retorna el libro especificado por el parametro **id**.

### Query Parameters
* **page:** especifica el numero de la pagina del libro a retornar. Si no se especifica una pagina, todas seran retornadas.
* **format:** especifica el formato de las paginas del libro a retornar. En esta primera iteracion, solo **text** (texto plano) y **html** estan soportados

Ejemplo:
```http://localhost:3000/api/books/5?page=2&format=html```